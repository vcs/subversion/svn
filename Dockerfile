FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7 AS builder

RUN yum install -y discount && mkdir -p /tmp/html
WORKDIR /tmp/html
COPY README.md style.md ./
RUN bash -c 'markdown -f FENCEDCODE -f NOSTYLE <( cat README.md style.md) > index.html'

FROM gitlab-registry.cern.ch/linuxsupport/cs8-base
MAINTAINER CERN IT-CDA-WF <web.services@cern.ch>

COPY repos/ /etc/yum.repos.d/
COPY httpd/ /etc/httpd/conf.d/

# Install required packages
RUN yum module switch-to subversion:1.14 -y && \
    yum install -y \
    CERN-CA-certs \
    enscript \
    epel-release \
    httpd \
    mod_dav_svn \
    mod_ldap \
    nss_wrapper \
    openssh-clients \
    php \
    php-mbstring \
    php-pear \
    php-xml \
    python3 \
    python3-subversion \
    rsync \
    subversion \
    subversion-tools \
    && \
    # Reinstalling glibc-common, so we can have all locales installed. Something necessary for some hooks
    yum -y reinstall glibc-common && \
    # Install geshi from epel-testing (not available elsewhere in CS8)
    yum -y --enablerepo=epel-testing install php-geshi && \
    # Install CERN-specific SVN packages from vcs7-stable koji tag
    yum -y --enablerepo=vcs7-stable install SVN-egroups && \
    # Make Python 3 available as "python"
    alternatives --set python /usr/bin/python3 && \
    yum clean all

# Install WebSVN
# https://github.com/websvnphp/websvn/releases
ARG WEBSVN_VERSION=2.6.1
ARG WEBSVN_SHA256=42299634127211744b5b9bfb1d0db4f3fa8ba73b3c0f3be8dc3ada68e6258037
RUN curl -sSL "https://github.com/websvnphp/websvn/archive/refs/tags/${WEBSVN_VERSION}.tar.gz" -o /tmp/websvn.tar.gz && \
    echo "${WEBSVN_SHA256}  /tmp/websvn.tar.gz" | sha256sum -c - && \
    mkdir -p /usr/share/websvn && \
    tar xzf "/tmp/websvn.tar.gz" --strip-components=1 -C /usr/share/websvn/ && \
    rm -f "/usr/share/websvn/README.md" && \
    rm -f "/tmp/websvn.tar.gz"

# Apply custom configuration for Apache and WebSVN
RUN mkdir /var/svn && \
    mkdir -p /run/httpd/ && chown -R root:root /run/httpd && chmod a+rw /run/httpd && \
    sed -i 's/Listen 80/# Listen 80\nListen 8000/' /etc/httpd/conf/httpd.conf && \
    # Logs to stdout and stderr
    sed -i 's#^\s*ErrorLog.*#ErrorLog /dev/stderr#' /etc/httpd/conf/httpd.conf && \
    sed -i 's#^\s*CustomLog.*#CustomLog /dev/stdout combined#' /etc/httpd/conf/httpd.conf && \
    # MPM, set the 'prefork' instead of 'event'. Otherwise php does not work
    sed -i.bak 's/^#LoadModule mpm_prefork_module/LoadModule mpm_prefork_module/' /etc/httpd/conf.modules.d/00-mpm.conf && \
    sed -i 's/^LoadModule mpm_event_module/#LoadModule mpm_event_module/' /etc/httpd/conf.modules.d/00-mpm.conf && \
    # Config.php, copy the default and add a single repository in /var/svn
    cp /usr/share/websvn/include/distconfig.php /usr/share/websvn/include/config.php && \
    echo "\$config->addRepository('repository', 'file:///var/svn/');" >>/usr/share/websvn/include/config.php && \
    echo "\$config->useAccessFile('/var/svn/conf/authz', 'repository');" >>/usr/share/websvn/include/config.php && \
    rm -rf /usr/share/httpd/noindex/*

COPY --from=builder /tmp/html/index.html /usr/share/httpd/noindex/
COPY bin/* /usr/local/bin/

EXPOSE 8000

USER 1001

CMD ["httpd", "-D", "FOREGROUND"]
