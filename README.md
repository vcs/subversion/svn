# SVN@Openshift

This is a self-service SVN repository server. While this SVN server is centrally hosted on via CERN Web Services,
**no support is provided for the self-service SVN server** beyond security updates and the web hosting environment to run the SVN server.
For a fully supported source code hosting service, use the [Git service](https://cern.service-now.com/service-portal/service-element.do?name=git-service).

This SVN server supports username and password HTTPS access only. SSH is not possible for the moment in Openshift. It also offers WebSVN to browse the code.

# Quick start

## Create an Openshift project and install the oc CLI

Please refer to the [PaaS user documentation](https://paas.docs.cern.ch/1._Getting_Started/1-create-paas-project/) for creating a new project on OKD4 PaaS and instructions how to obtain the Openshift client CLI `oc`.

## Create an empty SVN repository

Log in the __svn__ pod by doing:

```
oc login https://api.paas.okd.cern.ch
oc project <project>
oc rsh dc/svn
```

Once inside the pod run:

```
svnadmin create /var/svn
# Add your user read write access
echo -e "[/]\n<user> = rw" >> /var/svn/conf/authz
# Give permisisons to apache to read/write recursively to all child directories and files
chmod 777 -R /var/svn
# Ignore errors regarding /var/svn directory
```

## Copy a repository from AFS

In order to copy a repository from AFS you need as well to have the __oc__ command installed, and read access to the repository folder. If the repository is called $REPO, the folder will be __/afs/cern.ch/project/svn/reps/$REPO__. Use the _SVN project's librarian account_, who has read access to the repository folder, to access AFS (e.g. `kinit <librarian account>`).
The librarian account for a SVN repo can be found in the repo details in [CERNForge](https://cern.ch/forge) or by looking at the AFS permissions on the repo folder (`fs la /afs/cern.ch/project/svn/reps/$REPO`).

```
# Extract the pod name
POD_NAME=$(oc rsh dc/svn bash -c 'echo $HOSTNAME' | tr -d '\r')
# Rsync files from your repo on AFS to the pod volume
oc rsync /afs/cern.ch/project/svn/reps/$REPO/ $POD_NAME:/var/svn/
```

Once the data is copied, the hooks must be modified in order to make it work. Inside the pod:

```
cd /var/svn/
mv hooks hooks.old
mv usr-hooks hooks
```

The hook scripts must be adapted to the new path, so wherever there is a mention to __/afs/cern.ch/project/svn/reps/$REPO__ it must be changed to __/var/svn__.

### Copy a BIG repository from AFS

It is recommended to use this method, if the size of the current Subversion repository is bigger than ~10GB. The main objective for this is to avoid overloading the OpenShift filers. This method allows running the full featured `rsync` and not the reduced version that `oc` provides.

The procedure is the following:

* First launch a rsync client pod in your project:

```
# Login in OpenShift
oc login openshift.cern.ch
# Change to your project
oc project <project>
oc run rsync --image agonzale/rsync-client --rm -ti --restart=Never --overrides='
{
    "spec": {
        "containers": [
            {
                "stdin": true,
                "tty": true,
                "args": [ "bash" ],
                "name": "rsync",
                "image": "agonzale/rsync-client",
                "volumeMounts": [
                    {
                        "mountPath": "/var/svn",
                        "name": "repository"
                    }
                ]
            }
        ],
        "volumes": [
            {
                "name": "repository",
                "persistentVolumeClaim":{"claimName": "repository"}
            }
        ]
    }
}
'
```

This gives directly a prompt inside the pod with rsync and ssh installed, and the repository volume mounted.

* Then test the RSYNC of the data:

```
rsync -av <user>@lxplus.cern.ch:/afs/cern.ch/project/svn/reps/$REPO/ /var/svn/ --bwlimit=10000 -n
```

This will simulate the synchronization of *all* files in `/afs/cern.ch/project/svn/reps/$REPO/` to `/var/svn`. For example, if you need to exclude a directory or a file, you can use `--exclude=PATTERN`. For details about this and other options, please check [rsync's manual](https://linux.die.net/man/1/rsync).

Once you are satisfied with the tests, drop `-n` to do the actual synchronization:

```
rsync -av lxplus.cern.ch:/afs/cern.ch/project/svn/reps/$REPO/ /var/svn/ --bwlimit=10000
```

This will synchronize of *all* files in `/afs/cern.ch/project/svn/reps/$REPO/` with a maximun bandwidth of 10Mb/s to `/var/svn/`.

# E-groups integration

There is a cronjob that every hour will expand the users belonging to groups in the __authz__ file.

# Authentication

By default LDAP is used for authentication, so CERN accounts can be used as in __svn.cern.ch__. A "token" option is provided as well, this is intended in case CERN account passwords do not want to be used.

Please review the `httpd-conf` ConfigMap in the OpenShift Web Console or on the command-line with:

```
oc describe configmap/httpd-conf
```

When you want to use the token authentication instead of LDAP, you need to create the password file and replace the config map that holds the LDAP configuration:

```
Create random tokens with 35 characters
base64 /dev/urandom | head -c 35
# Create the htpasswd file
htpasswd -c passwords <user>
# Any password can be entered
# Change the files to the config map
oc export configmap httpd-conf -o json | jq ". * $(oc create configmap httpd-conf --from-file=tokens.conf --from-file=passwords --dry-run -o json)" | oc apply -f -
```

In order to add users you have to replace the __config map__ that holds the
__passwords__ file:

```
# Retrieve the current password file
oc export configmap httpd-conf -o json | jq '.data["passwords"]' -r >passwords
# Add new user, or replace its token
htpasswd passwords <otherus>
# Do the update
patch=$(jq -n --arg newparams "$(cat passwords| base64)" '{"data":{"passwords":$newparams}})'
oc patch configmap httpd-conf -p "$patch"
# Delete the passwords file from the local disk
rm passwords
```

# Development

## Container image tests

After the CI build the container image defined by `Dockerfile`, it automatically runs some basic tests on it using [container-structure-test](https://github.com/GoogleContainerTools/container-structure-test) according to `test/container-structure.yaml`.
Generally speaking, all test cases are there for a reason and should not be ignored!

## Integration tests

Additionally, this repository comes with integration tests for validating a new container image (see [.gitlab-ci.yml](.gitlab-ci.yml)).
The "svn-ci-integration-tests" project on the [OKD4 PaaS Staging cluster](https://paas-stg.cern.ch/) is used to run these tests.
In case this project get deleted (*it should not!*), run these commands to re-create it:

```sh
oc login --token=YOUR_TOKEN --server=https://api.paas-stg.okd.cern.ch
oc new-project "svn-ci-integration-tests" \
       --description "Project for running integration tests for SVN image (https://gitlab.cern.ch/vcs/subversion/svn) -- DO NOT DELETE" \
       --as jhensche --as-group system:authenticated:oauth --as-group system:authenticated
oc create serviceaccount svn-ci
oc policy add-role-to-user edit -z svn-ci
oc serviceaccounts get-token svn-ci
```

Add the token printed by the last command in the [CI variables settings](https://gitlab.cern.ch/vcs/subversion/svn/-/settings/ci_cd) as `CI_TEST_TOKEN` (check *Mask variable*).

Furthermore, a CERN service account called [svncitester](https://users-portal.web.cern.ch/identities/svncitester) is used to perform tests for the SSO integration.
The service account login needs to be stored in the CI variable `LDAP_USER_TEST` and the associated password in `LDAP_USER_PASSWORD` (should be a "Masked" variable).

The SVN instance created by the integration tests is exposed at <https://svn-ci-integration-tests.apptest.cern.ch/>.
The path `/repo` is authenticated by Apache with LDAP, the rest are authenticated with [cern-auth-proxy](https://gitlab.cern.ch/paas-tools/okd4-deployment/cern-auth-proxy/).

**Note**: when you use your own account to log into `/websvn`, you will likely get a `Server Error 500` despite being logged in with CERN SSO.
This is due to the fact that your account is not allowed to access the SVN repository.
You can change this by adding your username in the *authz* file inside the SVN container:

```
echo -e '[/]\nYOUR_USERNAME = rw' >> /var/svn/conf/authz
```

## Rollback to a previous image

First it is necessary to get the hash of the previous image. Using the `oc` command for the stable tag, would be like this:

```
oc login https://api.paas.okd.cern.ch
oc get imagestream/svn-server -o json | jq ' .status.tags | .[] | select(.tag=="stable") | .items'
# jq is used to show only the specific section we are interested in. But it is not necessary
```

This will output something like:
```
[
  {
    "created": "2019-04-04T14:15:36Z",
    "dockerImageReference": "docker-registry.default.svc:5000/your-project/svn-server@sha256:63ecde2cf8c840b86333807988f6547a252a68d54d831b340478c82c217ad63e",
    "generation": 35,
    "image": "sha256:63ecde2cf8c840b86333807988f6547a252a68d54d831b340478c82c217ad63e"
  },
  {
    "created": "2019-04-01T13:33:44Z",
    "dockerImageReference": "docker-registry.default.svc:5000/your-project/svn-server@sha256:e5c19391d7984619f98363713e5e7da12e7c908d355153a2c1c5dd3e63e9d459",
    "generation": 35,
    "image": "sha256:e5c19391d7984619f98363713e5e7da12e7c908d355153a2c1c5dd3e63e9d459"
  },
  {
    "created": "2019-02-28T16:11:52Z",
    "dockerImageReference": "docker-registry.default.svc:5000/your-project/svn-server@sha256:77d14e129f7444d6beb2f3961d89e9de1ae811d8d43c6dc891ea08110380a473",
    "generation": 35,
    "image": "sha256:77d14e129f7444d6beb2f3961d89e9de1ae811d8d43c6dc891ea08110380a473"
  },
(...)
```

Here we can see that the last stable version is from the 4th April 2019, and the previous one from 1st April 2019. As we are interested on the previous image, we should use its hash like this:

```
oc tag --reference-policy='local' gitlab-registry.cern.ch/vcs/subversion/svn@sha256:e5c19391d7984619f98363713e5e7da12e7c908d355153a2c1c5dd3e63e9d459 svn-server:stable
```

After tagging the previous image in the ImageStream, the SVN server will automatically be restarted with the "new" image.
