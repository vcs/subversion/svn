#!/bin/bash

set -e

JOB_NAME="sync-egroups-ci-test-${CI_JOB_ID}"

echo "Verifying that sync-egroups job works"
oc create job --from=cronjob/sync-egroups "$JOB_NAME"

TIMEOUT_IN_MINUTES=2
STOP_AT=$((SECONDS + TIMEOUT_IN_MINUTES * 60))
while [ "$(oc get job "$JOB_NAME" -o jsonpath='{.status.succeeded}')" != "1" ]; do
    echo "Waiting for job ${JOB_NAME} to complete..."
    if [ "$SECONDS" -gt "$STOP_AT" ]; then
        echo "Job failed to complete successfully in ${TIMEOUT_IN_MINUTES} minutes".
        oc delete job "$JOB_NAME"
        exit 1
    fi
    sleep 10
done

oc delete job "$JOB_NAME"
