#!/bin/bash
set -e

# Create SVN repository
echo " * Create SVN repository"
oc rsh dc/svn svnadmin create /var/svn/ || true
#
echo " * Add permission to account $LDAP_USER_TEST"
oc rsh dc/svn bash -c "echo -e '[/]\n$LDAP_USER_TEST = rw' >>/var/svn/conf/authz"
#
echo " * Install commit hooks"
oc rsh dc/svn bash -c 'cat - > /var/svn/hooks/pre-commit <<EOF
#!/bin/sh
# A simple pre-commit hook that uses Python
REPOS="\$1"
TXN="\$2"
/var/svn/hook-scripts/case-insensitive.py "\$REPOS" "\$TXN" || exit 1
exit 0
EOF
mkdir -p /var/svn/hook-scripts/
curl -sL https://svn.apache.org/repos/asf/subversion/trunk/contrib/hook-scripts/case-insensitive.py -o /var/svn/hook-scripts/case-insensitive.py
chmod +x /var/svn/hook-scripts/case-insensitive.py /var/svn/hooks/pre-commit
'
#
echo " * Getting hostname"
hostname=$(oc get route/cern-auth-proxy -o go-template --template '{{.spec.host}}')
#
echo " * Ping WebSVN endpoint, must be authenticated * "
http_status="$(curl -o /dev/null -s -w "%{http_code}\n" https://${hostname}/websvn)"
if [ "$http_status" -ne 302 ]; then
    echo "Expected HTTP status 302, got $http_status"
    exit 1
fi
#
echo " * Ping SVN repository, must be authenticated * "
http_status="$(curl -o /dev/null -s -w "%{http_code}\n" https://${hostname}/repo)"
if [ "$http_status" -ne 401 ]; then
    echo "Expected HTTP status 401, got $http_status"
    exit 1
fi
#
echo " * List contents of the repository, must be empty"
svn ls "https://${hostname}/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * Checkout the repository"
svn co "https://${hostname}/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * Add file date"
cd repo
#
date >date.txt
#
svn add date.txt
#
echo " * Commiting the new file"
svn ci -m "Add test date" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST" --non-interactive
#
echo " * See that the contents are the expected"
test "$(svn ls "https://${hostname}/repo" --username "$LDAP_USER_TEST" --password "$LDAP_PASSWORD_TEST")" = "date.txt"
#
echo " * DONE *"
