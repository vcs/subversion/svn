#!/bin/bash

set -e

# Remove any left-over jobs from previous CI pipelines
oc delete jobs --all

# Remove resources associated to cern-auth-proxy Helm chart
helm uninstall cern-auth-proxy || true

# Remove all the elements created by the template to have a fresh start
oc delete deploymentconfig,route,service,configmap,serviceaccount,persistentvolumeclaim,cronjob,imagestream -l template=svn-server

# Wait until the pvc is completely deleted to proceed
while oc get pvc repository 2> /dev/null
do
    sleep 5s
done
