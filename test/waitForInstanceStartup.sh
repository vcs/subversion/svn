#!/bin/bash -x

set -e

# wait until the SVN instance starts successfully or TIMEOUT_IN_MINUTES have elapsed
TIMEOUT_IN_MINUTES=${TIMEOUT_IN_MINUTES:-10}

# break on error messages in SVN startup log if value is YES
BREAK_ON_ERRORS=${BREAK_ON_ERRORS:-YES}

# print log if value is YES
PRINT_STARTUP_LOG=${PRINT_STARTUP_LOG:-YES}

if [ "${PRINT_STARTUP_LOG}" == "YES" ]; then
  trap 'echo "SVN startup log dump:"; echo "${log}"' EXIT
fi

STOP_AT=$((SECONDS + TIMEOUT_IN_MINUTES * 60))
SLEEP_INTERVAL=${SLEEP_INTERVAL:-30}

while [ "${SECONDS}" -lt "${STOP_AT}" ]; do
  # Deployment usually takes a couple minutes.
  # If we're running just after a redeployment command then we might not immediately see the latest deployment
  # so wait first before polling for deployment logs.
  sleep "${SLEEP_INTERVAL}s";
  log=$(oc logs dc/svn || true)

  # check for startup success
  hostname=$(oc get route/svn -o go-template --template '{{.spec.host}}')

  if [ "$(curl -qs "https://${hostname}/" -w "%{http_code}" -o /dev/null)" -eq '403' ];
  then
    echo 'SVN is fully up and running'
    exit 0
  fi

  echo "SVN instance is not ready yet, trying again in ${SLEEP_INTERVAL} seconds...";
done

echo "Failure to get a running SVN instance in ${TIMEOUT_IN_MINUTES} minutes"
exit 1
